
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object helloworld extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""<h1>Play Framework With Scala</h1>
<h2>"""),_display_(/*3.6*/message),format.raw/*3.13*/("""</h2>

"""),_display_(/*5.2*/index("Scala World!!")))
      }
    }
  }

  def render(message:String): play.twirl.api.HtmlFormat.Appendable = apply(message)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (message) => apply(message)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-01-14T13:49:30.777
                  SOURCE: /Users/dmodem/Documents/workspace/scala-play/HelloWorldSample/app/views/helloworld.scala.html
                  HASH: 21d5afa98fb64c1db9d4b6f6c1ed874e5c05374c
                  MATRIX: 734->1|845->19|910->59|937->66|970->74
                  LINES: 21->1|26->2|27->3|27->3|29->5
                  -- GENERATED --
              */
          