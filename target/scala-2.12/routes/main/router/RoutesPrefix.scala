// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/dmodem/Documents/workspace/scala-play/HelloWorldSample/conf/routes
// @DATE:Tue Jan 14 13:49:30 CST 2020


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
