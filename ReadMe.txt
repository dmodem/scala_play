> Install Scala and Play plugins in intellij(ultimate edition)
> Import this project as sbt project and choose to run on sbt shell.
> Under sbt tab to the right in intellij, choose run from sbt tasks under this project

> When application is running you will see below lines in the sbt shell:
--- (Running the application, auto-reloading is enabled) ---

[info] p.c.s.AkkaHttpServer - Listening for HTTP on /0:0:0:0:0:0:0:0:9000

> And we can access the api (end points can be found in routes file) from
http://localhost:9000/