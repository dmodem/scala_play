package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

class HelloWorldController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
  def helloworld = Action { Ok(views.html.helloworld("Your new application is working!!.")) }

  def welcome = Action { Ok(views.html.index("Play Framework!!")) }
}
